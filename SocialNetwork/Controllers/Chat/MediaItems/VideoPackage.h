//
//  VideoPackage.h
//  D'Netzwierk
//
//  Created by Michael Griffin.
//  Copyright (c) 2018 Michael Griffin. All rights reserved.
//

#import "JSQMediaItem.h"

@interface VideoPackage : JSQMediaItem <JSQMessageMediaData, NSCoding, NSCopying>
 
@property (nonatomic, strong) NSURL *fileURL;
@property (nonatomic, assign) BOOL rdyToPlay;

@property (copy, nonatomic) UIImage *img;

- (instancetype)initWithFileURL:(NSURL *)fileURL rdyToPlay:(BOOL)rdyToPlay;

@end
