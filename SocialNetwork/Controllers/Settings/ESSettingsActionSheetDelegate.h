//
//  ESSettingsActionSheetDelegate.h
//  D'Netzwierk
//
//  Created by Michael Griffin.
//  Copyright (c) 2018 Michael Griffin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESSettingsActionSheetDelegate : NSObject <UIActionSheetDelegate>

/// Navigation controller of calling view controller
@property (nonatomic, strong) UINavigationController *navController;

- (id)initWithNavigationController:(UINavigationController *)navigationController;

@end
